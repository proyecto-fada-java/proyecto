/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controllers;

import Models.ProcedimientoModel;
import Utils.Utils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author jeffrey.rios
 */
public class ProcedimientoController {

    private String datosDelProblema;
    private List<ProcedimientoModel> procedimientos;
    private int cantidadProcedimientos;
    private String solucionProblema;

    public ProcedimientoController(String datosDelProblema) {
        procedimientos = new ArrayList<ProcedimientoModel>();
        this.datosDelProblema = datosDelProblema;
        InicializarDatosDelProblema();
    }

    public String getSolucionProblema() {
        return solucionProblema;
    }

    private void InicializarDatosDelProblema() {
        try {
            String[] datosDelProblema = this.datosDelProblema.split("\r?\n|\r");
            ObtenerOtrosDatos(datosDelProblema);
            ObtenerProcedimientos(datosDelProblema);
        } catch (Exception ex) {
            throw ex;
        }
    }

    private void ObtenerProcedimientos(String[] datosDelProblema) {
        ProcedimientoModel procedimiento;

        for (int i = 1; i < datosDelProblema.length; i++) {
            String[] cadenaSplit = datosDelProblema[i].split(",");

            procedimiento = new ProcedimientoModel(
                    cadenaSplit[0],
                    Utils.HoraASeguntos(cadenaSplit[1]),
                    Utils.HoraASeguntos(cadenaSplit[2]),
                    0
            );
            procedimiento.duracionEnSegundos = procedimiento.horaFinEnSegundos - procedimiento.horaInicioEnSegundos;
            this.procedimientos.add(procedimiento);
        }
    }

    private void ObtenerOtrosDatos(String[] datosDelProblema) {
        cantidadProcedimientos = Integer.parseInt(datosDelProblema[0]);
    }

    private int findLastNonConflictingJob(List<ProcedimientoModel> jobs, int n) {
        // search space
        int low = 0;
        int high = n;

        // iterate till the search space is exhausted
        while (low <= high) {
            int mid = (low + high) / 2;
            if (jobs.get(mid).horaFinEnSegundos <= jobs.get(n).horaInicioEnSegundos) {
                if (jobs.get(mid + 1).horaFinEnSegundos <= jobs.get(n).horaInicioEnSegundos) {
                    low = mid + 1;
                } else {
                    return mid;
                }
            } else {
                high = mid - 1;
            }
        }

        // return the negative index if no non-conflicting job is found
        return -1;
    }

    // Function to print the non-overlapping procedimientos involved in maximum profit
    // using dynamic programming
    private void findMaxProfitJobs(List<ProcedimientoModel> procedimientos) {
        // sort procedimientos in increasing order of their finish times
        Collections.sort(procedimientos, Comparator.comparingInt(x -> x.horaFinEnSegundos));

        // get the number of procedimientos
        int n = procedimientos.size();

        // base case
        if (n == 0) {
            return;
        }

        // `maxProfit[i]` stores the maximum profit possible for the first `i` procedimientos,
        // and `tasks[i]` stores the index of procedimientos involved in the maximum profit
        int[] maxProfit = new int[n];

        List<List<Integer>> procedimientosEscogidos = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            procedimientosEscogidos.add(new ArrayList<>());
        }

        // initialize `maxProfit[0]` and `tasks[0]` with the first job
        maxProfit[0] = procedimientos.get(0).duracionEnSegundos;
        procedimientosEscogidos.get(0).add(0);

        // fill `tasks[]` and `maxProfit[]` in a bottom-up manner
        for (int i = 1; i < n; i++) {
            // find the index of the last non-conflicting job with the current job
            int index = findLastNonConflictingJob(procedimientos, i);

            // include the current job with its non-conflicting procedimientos
            int currentProfit = procedimientos.get(i).duracionEnSegundos;
            if (index != -1) {
                currentProfit += maxProfit[index];
            }

            // if including the current job leads to the maximum profit so far
            if (maxProfit[i - 1] < currentProfit) {
                maxProfit[i] = currentProfit;

                if (index != -1) {
                    procedimientosEscogidos.set(i, new ArrayList<>(procedimientosEscogidos.get(index)));
                }
                procedimientosEscogidos.get(i).add(i);
            } // excluding the current job leads to the maximum profit so far
            else {
                procedimientosEscogidos.set(i, new ArrayList<>(procedimientosEscogidos.get(i - 1)));
                maxProfit[i] = maxProfit[i - 1];
            }
        }

        GenerarSolucionProblema(procedimientosEscogidos, maxProfit[n - 1]);

    }
    
      public void findMaxProfitJobs2(List<ProcedimientoModel> procedimientos)
    {
        // sort jobs in increasing order of their finish times
        Collections.sort(procedimientos, Comparator.comparingInt(x -> x.horaFinEnSegundos));
 
        // get the number of jobs
        int n = procedimientos.size();
 
        // base case
        if (n == 0) {
            return;
        }
 
        // `maxProfit[i]` stores the maximum profit possible for the first `i` jobs,
        // and `tasks[i]` stores the index of jobs involved in the maximum profit
        int[] maxProfit = new int[n];
 
        List<List<Integer>> tasks = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            tasks.add(new ArrayList<>());
        }
 
        // initialize `maxProfit[0]` and `tasks[0]` with the first job
        maxProfit[0] = procedimientos.get(0).duracionEnSegundos;
        tasks.get(0).add(0);
 
        // fill `tasks[]` and `maxProfit[]` in a bottom-up manner
        for (int i = 1; i < n; i++)
        {
            // find the index of the last non-conflicting job with the current job
            int index = findLastNonConflictingJob(procedimientos, i);
 
            // include the current job with its non-conflicting jobs
            int currentProfit = procedimientos.get(i).duracionEnSegundos;
            if (index != -1) {
                currentProfit += maxProfit[index];
            }
 
            // if including the current job leads to the maximum profit so far
            if (maxProfit[i-1] < currentProfit)
            {
                maxProfit[i] = currentProfit;
 
                if (index != -1) {
                    tasks.set(i, new ArrayList<>(tasks.get(index)));
                }
                tasks.get(i).add(i);
            }
 
            // excluding the current job leads to the maximum profit so far
            else {
                tasks.set(i, new ArrayList<>(tasks.get(i-1)));
                maxProfit[i] = maxProfit[i-1];
            }
        }
 
        // `maxProfit[n-1]` stores the maximum profit
        System.out.println("The maximum profit is " + maxProfit[n-1]);
 
        // `tasks[n-1]` stores the index of jobs involved in the maximum profit
        System.out.print("The jobs involved in the maximum profit are ");
        for (int i: tasks.get(n-1)) {
            System.out.print(procedimientos.get(i).nombre);
        }
    }

    private void GenerarSolucionProblema(List<List<Integer>> procedimientosEscogidos, long maximoBeneficio) {
        solucionProblema = String.valueOf(procedimientosEscogidos.get(procedimientos.size() - 1).size()) + "\n";
//        solucionProblema += String.valueOf(maximoBeneficio) + "\n";
         solucionProblema += Utils.SecondsToHoursAndMinutes(maximoBeneficio) + "\n";

        for (int i : procedimientosEscogidos.get(procedimientos.size() - 1)) {
            solucionProblema += procedimientos.get(i).nombre 
                    + Utils.SecondsToHoursAndMinutes(procedimientos.get(i).duracionEnSegundos) + "\n";
        }
    }

    public void EjecutarSolucionDinamica() {
        findMaxProfitJobs(procedimientos);
        findMaxProfitJobs2(procedimientos);
    }

}
