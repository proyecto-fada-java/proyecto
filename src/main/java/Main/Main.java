/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package Main;

import Views.MDI;

/**
 *
 * @author jeffrey.rios
 */
public class Main {

    private static MDI pubObjMDI;

    public static void main(String[] args) {
        pubObjMDI = new MDI();
        pubObjMDI.setVisible(true);
    }
}
