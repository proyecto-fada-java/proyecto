/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

import java.math.BigDecimal;

/**
 *
 * @author jeffrey.rios
 */
public class ProcedimientoModel {

    public String nombre;
    public int horaInicioEnSegundos;
    public int horaFinEnSegundos;
    public int duracionEnSegundos;

    public ProcedimientoModel(String nombre, int horaInicioEnSegundos, int horaFinEnSegundos, int duracionEnSegundos) {
        this.nombre = nombre;
        this.horaInicioEnSegundos = horaInicioEnSegundos;
        this.horaFinEnSegundos = horaFinEnSegundos;
        this.duracionEnSegundos = duracionEnSegundos;
    }
}
