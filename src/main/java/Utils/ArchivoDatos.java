/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Utils;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author jeffrey.rios
 */
public class ArchivoDatos {

    private String rutaAbsoluta;
    private String datos;

    public ArchivoDatos(String rutaAbsoluta) {
        this.rutaAbsoluta = rutaAbsoluta;
    }

    public String getDatos() {
        return datos;
    }

    public void LeerArchivoComoString() throws Exception {
        try {
            datos = new String(Files.readAllBytes(Paths.get(rutaAbsoluta)));
        } catch (Exception e) {
            throw e;
        }
    }

    public void EscribirArchivo(String datos, String tipoProblema) throws Exception {
        try {
            String fileName = "Solucion" + tipoProblema + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+".txt";

            Path filePath = Paths.get(System.getProperty("user.dir"),"pruebas", "soluciones");
            
            String absolutePath = filePath +"/"+ fileName;

            FileWriter myWriter = new FileWriter(absolutePath);

            myWriter.write(datos);
            myWriter.close();
        } catch (Exception e) {
            throw e;
        }
    }
}
