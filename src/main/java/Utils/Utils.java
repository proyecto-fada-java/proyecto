/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Utils;

import java.math.BigDecimal;

/**
 *
 * @author jeffrey.rios
 */
public class Utils {

    public static int HoursToSeconds(int hours) {
        return hours * 60 * 60;
    }

    public static int MinutesToSeconds(int minutes) {
        return minutes * 60;
    }

    public static String SecondsToHoursAndMinutes(long seconds) {
        double hours = Math.floor(seconds / 3600);
        double minutes = Math.floor((seconds - hours * 3600) / 60);

        return String.valueOf(hours) + "h " + String.valueOf(minutes) + "m";
    }

    public static int HoraASeguntos(String hora) {        
        String[] objHora = hora.split(":");
        int horaEnSegundos = HoursToSeconds(Integer.parseInt(objHora[0]));
        int minutosEnSegundos = HoursToSeconds(Integer.parseInt((objHora[1])));
        int horaFinalSegundos = horaEnSegundos + minutosEnSegundos;

        return horaFinalSegundos;
    }

}
